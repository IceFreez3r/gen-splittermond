/**
 *
 */
package org.prelle.splimo.charctrl;

/**
 * @author prelle
 *
 */
public interface CreatureTrainerController extends CommonCreatureController {

	//-------------------------------------------------------------------
	public int getAvailablePotential();
	
}
