/**
 * 
 */
package org.prelle.splimo.modifications;

import org.prelle.simplepersist.EnumValue;

/**
 * @author prelle
 *
 */
public enum ModificationValueType {

	@EnumValue("rel")
	RELATIVE,
	@EnumValue("abs")
	ABSOLUT,
	
}
