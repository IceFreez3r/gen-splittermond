Dies ist der Quellcode und die XML-Daten für das *Splittermond*-Plugin des Projekts **RPGFramework**.

Dieses Plugin ist ein Fanprojekt, welches Verwendung findet in der Charakterverwaltungssoftware **Genesis**.
Wir begrüßen ausdrücklich, wenn Leute uns hier [unterstützen wollen](https://rpgframework.atlassian.net/wiki/spaces/GD/pages/307167443/Helping+the+project) - sei es als Entwickler oder Datenredakteur.

Mit der Ausnahme der Daten aus den Splittermond-Büchern ist dies ein Open Source Projekt. Ihr könnt den Programmcode oder die Produktdaten für eigene Open Source Projekte weiterverwenden. Auf Anfrage ist es auch möglich die Splittermond-Daten für eure Open Source Projekte oder freien Community-Projekte zu verwenden, allerdings muss dabei die Einhaltung der [Fanrichtlinien](http://www.splitterwiki.de/wiki/Richtlinie) gewährleistet sein.